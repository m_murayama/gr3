<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	List<String> nameList = Arrays.asList("A", "B", "C");
	request.setAttribute("nameList", nameList);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<ul>
	<c:forEach items="${nameList}" var="name">
		<li><c:out value="${name}"></c:out></li>
	</c:forEach>
	</ul>
</body>
</html>